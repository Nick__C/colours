import './App.css';
import {useState} from "react";

function App() {
    const [colourValues, setColourValues] = useState([]);

    const generateColours = () => {
        numbers()
  return (
      colourValues.map(value => <div style={'backgroundColor:' + value + '; width: 1px; height: 1px;'}/>)
  )}


    function numbers(){
        let colours = []
        for(let i=0; i < 1<<4; i++) {
            let r = (i>>2) & 0xff;
            let g = (i>>1) & 0xff;
            let b = i & 0xff;
            const newColour = "rgb("+r+","+g+","+b+")";
            colours.push([newColour])
        }
        setColourValues(colours);
    }

  return (
    <div className="App">
      {generateColours()}
    </div>
  );
}

export default App;
